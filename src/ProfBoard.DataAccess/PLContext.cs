﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ProfBoard.DataAccess.Models;

namespace ProfBoard.DataAccess
{
    public partial class PLContext : DbContext
    {
        public PLContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
#if DEBUG
            optionsBuilder.UseSqlite("Filename=PLDB.db");
            optionsBuilder.EnableSensitiveDataLogging();
#endif

        }

        public virtual DbSet<AgentSetting> AgentSettings { get; set; } = null!;
        public virtual DbSet<BooleanValue> BooleanValues { get; set; } = null!;
        public virtual DbSet<ByteValue> ByteValues { get; set; } = null!;
        public virtual DbSet<DintValue> DintValues { get; set; } = null!;
        public virtual DbSet<DwordValue> DwordValues { get; set; } = null!;
        public virtual DbSet<FloatValue> FloatValues { get; set; } = null!;
        public virtual DbSet<InserterReport> InserterReports { get; set; } = null!;
        public virtual DbSet<IntValue> IntValues { get; set; } = null!;
        public virtual DbSet<PlcCpuType> PlcCpuTypes { get; set; } = null!;
        public virtual DbSet<PlcDataType> PlcDataTypes { get; set; } = null!;
        public virtual DbSet<PlcTableSetting> PlcTableSettings { get; set; } = null!;
        public virtual DbSet<PlcVarType> PlcVarTypes { get; set; } = null!;
        public virtual DbSet<PlcVaribleSetting> PlcVaribleSettings { get; set; } = null!;
        public virtual DbSet<StringValue> StringValues { get; set; } = null!;
        public virtual DbSet<WordValue> WordValues { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AgentSetting>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.Id, "ClusteredIndex-20190609-001734")
                    .IsClustered();
            });

            modelBuilder.Entity<BooleanValue>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.AgentDateTime, "ClusteredIndex-20190608-193717")
                    .IsClustered();

                entity.Property(e => e.AgentDateTime).HasColumnType("datetime");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.InsertDateTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<ByteValue>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.AgentDateTime, "ClusteredIndex-20190608-224851")
                    .IsClustered();

                entity.Property(e => e.AgentDateTime).HasColumnType("datetime");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.InsertDateTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<DintValue>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.AgentDateTime, "ClusteredIndex-20190608-224050")
                    .IsClustered();

                entity.Property(e => e.AgentDateTime).HasColumnType("datetime");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.InsertDateTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<DwordValue>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.AgentDateTime, "ClusteredIndex-20190607-135126")
                    .IsClustered();

                entity.Property(e => e.AgentDateTime).HasColumnType("datetime");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.InsertDateTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<FloatValue>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.AgentDateTime, "ClusteredIndex-20190608-023235")
                    .IsClustered();

                entity.Property(e => e.AgentDateTime).HasColumnType("datetime");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.InsertDateTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<InserterReport>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.BeginTime, "ClusteredIndex-20190608-235533")
                    .IsClustered();

                entity.Property(e => e.BeginTime).HasColumnType("datetime");

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<IntValue>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.AgentDateTime, "ClusteredIndex-20190608-225356")
                    .IsClustered();

                entity.Property(e => e.AgentDateTime).HasColumnType("datetime");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.InsertDateTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<PlcCpuType>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.Id, "ClusteredIndex-20190609-001808")
                    .IsClustered();
            });

            modelBuilder.Entity<PlcDataType>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.Id, "ClusteredIndex-20190609-001825")
                    .IsClustered();
            });

            modelBuilder.Entity<PlcTableSetting>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.Id, "ClusteredIndex-20190609-001845")
                    .IsClustered();
            });

            modelBuilder.Entity<PlcVarType>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.Id, "ClusteredIndex-20190609-001934")
                    .IsClustered();
            });

            modelBuilder.Entity<PlcVaribleSetting>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.Id, "ClusteredIndex-20190609-001901")
                    .IsClustered();

                entity.Property(e => e.Db).HasColumnName("DB");

                entity.Property(e => e.Dbserializeble).HasColumnName("DBSerializeble");

                entity.Property(e => e.Info).HasColumnType("text");
            });

            modelBuilder.Entity<StringValue>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.AgentDateTime, "ClusteredIndex-20190608-225745")
                    .IsClustered();

                entity.Property(e => e.AgentDateTime).HasColumnType("datetime");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.InsertDateTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<WordValue>(entity =>
            {
                entity.HasNoKey();

                entity.HasIndex(e => e.AgentDateTime, "ClusteredIndex-20190608-230108")
                    .IsClustered();

                entity.Property(e => e.AgentDateTime).HasColumnType("datetime");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.InsertDateTime).HasColumnType("datetime");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
