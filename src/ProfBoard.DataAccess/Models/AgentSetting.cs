﻿using System;
using System.Collections.Generic;

namespace ProfBoard.DataAccess.Models
{
    public partial class AgentSetting
    {
        public long Id { get; set; }
        public string Name { get; set; } = null!;
        public int AgentTypeCode { get; set; }
        public string? Location { get; set; }
        public string? ServiceName { get; set; }
        public string? Description { get; set; }
    }
}
