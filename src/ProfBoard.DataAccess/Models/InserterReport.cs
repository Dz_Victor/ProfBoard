﻿using System;
using System.Collections.Generic;

namespace ProfBoard.DataAccess.Models
{
    public partial class InserterReport
    {
        public long Id { get; set; }
        public DateTime BeginTime { get; set; }
        public DateTime EndTime { get; set; }
        public int Duration { get; set; }
        public int? RecordCount { get; set; }
        public int QueueSize { get; set; }
        public short SourceId { get; set; }
    }
}
