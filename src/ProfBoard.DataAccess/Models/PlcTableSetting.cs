﻿using System;
using System.Collections.Generic;

namespace ProfBoard.DataAccess.Models
{
    public partial class PlcTableSetting
    {
        public long Id { get; set; }
        public string Name { get; set; } = null!;
        public long AgentSettingId { get; set; }
        public int CpuTypeCode { get; set; }
        public string ControlerAddress { get; set; } = null!;
        public int Rack { get; set; }
        public int Slot { get; set; }
        public int IntervalMs { get; set; }
        public bool AutoStart { get; set; }
        public string Description { get; set; } = null!;
        public string? SourceFilter { get; set; }
    }
}
