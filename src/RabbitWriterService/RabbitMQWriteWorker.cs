using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RabbitWriterService
{
    public class RabbitMQWriteWorker : BackgroundService
    {
        private readonly ILogger<RabbitMQWriteWorker> _logger;
        private IConnection _connection;
        private IModel _channel;
        private readonly List<ControllerOption> _options;
        private string _hostName;

        public RabbitMQWriteWorker(ILogger<RabbitMQWriteWorker> logger, List<ControllerOption> options, string hostName)
        {
            _logger = logger;
            _hostName = hostName;
            _options = options;
            InitRabbitMQ();
        }

        public void Send(string queue, string data)
        {
           _channel.QueueDeclare(queue, false, false, false, null);
           _channel.BasicPublish(string.Empty, queue, null, Encoding.UTF8.GetBytes(data));
           _logger.LogInformation($"Write data: {queue} : {data}");
        }

        private void InitRabbitMQ()
        {
            var factory = new ConnectionFactory{ HostName = _hostName };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
        }
        
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Random rnd = new Random();
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                foreach (var option in _options ) 
                {
                    Send(option.Name, NextDouble(rnd, option.MinRangeLimit, option.MaxRangeLimit).ToString());
                }
                await Task.Delay(_options[0].PeriodAutoResend, stoppingToken);
            }
        }

        private double NextDouble(Random rand, double minValue, double maxValue)
        {
            return rand.NextDouble() * (maxValue - minValue) + minValue;
        }
    }
}
